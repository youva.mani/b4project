#!/bin/sh
cd ./send_morse
make clean
make
cd ../receive_morse
make clean
make
cd ..
sudo rmmod send_morse.ko
sudo rmmod receive_morse.ko
sudo insmod ./send_morse/send_morse.ko
sudo insmod ./receive_morse/receive_morse.ko
gcc ./app/morse.c -o ./app/morse
