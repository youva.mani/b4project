#include <linux/module.h> /* Needed by all modules */
#include <linux/kernel.h> /* Needed for KERN_INFO */
#include <linux/init.h> /* Needed for the macros */
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/string.h>
#include "./../../headers/send_morse.h"
#include <linux/ktime.h>
#include <linux/timer.h>
#include <linux/gpio.h>
#include <linux/delay.h>


static struct file_operations fops = 
{
	.read = my_morse_read,
	.open = my_morse_open,
	.write = my_morse_write,
	.release = my_morse_rls, 
};

/***************************************
 *  envoi des données binaires vers les
 *  GPIO de la raspberry
 * ************************************/

int send_morse (void)
{
	if (luck == 1)
	{
		int y;
		for( y=0 ; y < count_bin ; y++)
		{
			printk(KERN_CRIT "  %i",bbuf[y]);
			gpio_set_value(RPI_GPIO_OUT, bbuf[y]);
			msleep(200);
		}
		luck=0;
		return 1;
	}else 
	{
		return 0;
	}	
}

/***************************************
 *  Conversion des caractères envoyé par
 *  l'application en binaire 
 * ************************************/

int morse_to_bin(void)
{
	taille_char = strlen(kbuf);
	if(luck == 0)
	{
		count_bin=0;
		for( i=0 ; i < taille_char ; i++)
	   	{	
			if (kbuf[i]=='-')
			{
				bbuf[count_bin]=code_1[0];
				bbuf[count_bin+1]=code_1[1];
				bbuf[count_bin+2]=code_1[2];
				bbuf[count_bin+3]=code_1[3];
				count_bin = count_bin +4;
			}
			else if (kbuf[i]=='.')
			{
				bbuf[count_bin]=code_2[0];
				bbuf[count_bin+1]=code_2[1];
				count_bin = count_bin+2;
			}
			else if (kbuf[i]==' ')
			{
				bbuf[count_bin]=code_3[0];
				bbuf[count_bin+1]=code_3[1];
				count_bin = count_bin+2;
			}
			else if (kbuf[i]=='/')
			{
				bbuf[count_bin]=code_4[0];
				bbuf[count_bin+1]=code_4[1];
				bbuf[count_bin+2]=code_4[2];
				bbuf[count_bin+3]=code_4[3];
				bbuf[count_bin+4]=code_4[4];
				bbuf[count_bin+5]=code_4[5];
				count_bin = count_bin +6;
			}
	   	}
		luck=1;
		sluck=0;
		while(0==send_morse ());

		return 1;
	}else
	{ 
		return 0;	
	}

}



// pour plus d'information sur l'initialisation des gpio 
//https://www.blaess.fr/christophe/2012/11/26/les-gpio-du-raspberry-pi/

/***************************************
 *  Initialisation et chargement du 
 *  module 
 * ************************************/

static int __init send_morse_init (void) 
{
	printk (KERN_CRIT " <....> initialisation  \n");
	err= register_chrdev(90,"My_Morse", &fops);
	if (err<0)printk(KERN_ALERT " <  NO> registration\n");
	else printk(KERN_ALERT "<OK  >registration\n");
	
	gpio_request(RPI_GPIO_OUT,THIS_MODULE->name);
	gpio_direction_output(RPI_GPIO_OUT,0);
	gpio_export(RPI_GPIO_OUT,0);
	
	printk (KERN_CRIT " <OK  > initialisation \n");

	return 0;
}

/***************************************
 *  déchargement du module
 * ************************************/

static void __exit send_morse_quit (void) 
{
	printk (KERN_CRIT " <....> déchargement du module \n");
	unregister_chrdev (90,"My_Morse");
	gpio_free(RPI_GPIO_OUT);
	printk (KERN_CRIT " <OK  > déchargement du module \n");
}

/***************************************
 *  Gestion du device associé au module 
 * ************************************/

static ssize_t my_morse_read (struct file * fil, char * buff, size_t len, loff_t * off)
{
	ssize_t ret = 0;
	printk (KERN_ALERT "read\n");
	if (*off == 0) 
	{
		if (copy_to_user(buff, kbuf, sizeof(kbuf))) 
		{
			ret = -EFAULT;
		} else 
		{
			ret = sizeof(kbuf);
			*off = 1;
		}
	}
	return ret;
}

static int my_morse_open (struct inode * inod, struct file * fil)
{
	printk (KERN_ALERT "open\n");
	return 0;
}

static ssize_t my_morse_write (struct file * fil, const char * buff, size_t len, loff_t * off)
{
	int l = 0, m, mylen;
	printk (KERN_ALERT "write ");
	if (sluck == 0)
	{	
		sluck=1 ;  // bocker le buffer kbuf pour ne pas recevoir des caractères de l'application
		if (len > taille_max_buffer - 1) 
		{
			mylen = taille_max_buffer - 1;
		} else 
		{
			mylen = len ;
		}
		for (l = 0; l < mylen ; l++) 
		{
			get_user (kbuf[l], buff + l);
		}
		for (m = mylen; m < taille_max_buffer; m++) 
		{
			kbuf[m] = 0;
		}
		printk (KERN_ALERT "%s \n", kbuf);
		while (0==morse_to_bin());
		return len ;
	}else
	{ 
		return -1;
	}
}

static int my_morse_rls (struct inode * inod, struct file * fil)
{
	printk (KERN_ALERT "bye\n");
	return 0;
}





module_init (send_morse_init);
module_exit (send_morse_quit);

MODULE_AUTHOR("MANI_AITSAID");
MODULE_DESCRIPTION("Module de transmission de code morse");
MODULE_LICENSE("GPL");





