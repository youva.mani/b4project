#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <linux/string.h>
#include <ctype.h>
#include <string.h>
#include <sys/ioctl.h>
#include <fcntl.h>



#define N_buffer 256
#define SIZE_LT 37

#define MYDRBASE 'k'
#define MYDR_RESET _IO( MYDRBASE, 1)
#define MYDR_STOP _IO( MYDRBASE, 2)


///////////////////////////////////////////////////////////////////////////////////////////////////
/*********************************** Function headers  ******************************************/
//////////////////////////////////////////////////////////////////////////////////////////////////
void StringToMorse(char *, char *);
void input_Message(char * );
void MorseToString(char*, char*);
void flushstring(char*);

/****************** Morse Table ********************/
char *morse_table[] = {
	"-----",".----","..---","...--",//Nombres
	"....-",".....","-....","--...",
	"---..","----.",   //Fin des Nombres
	".-","-...","-.-.","-..",".",//Alphabet
	"..-.","--.","....","..",".---","-.-"
	,".-..","--","-.","---",".--.",
	"--.-",".-.","...","-","..-",
	"...-",".--","-..-","-.--","--..","/"};//Fin de l'alphabet
char raw_table[] = {
	'0','1','2','3','4','5','6','7','8','9',
	'A','B','C','D','E','F','G','H','I','J',
	'K','L','M','N','O','P','Q','R','S','T',
	'U','V','W','X','Y','Z',' '};


int main()
{
	/* Communication descriptors */
	int fd;
	int rd;
	int ret_val;



	char *Msg_raw;
	char *Msg_Morse;

	Msg_raw = malloc(N_buffer * sizeof(char));
	Msg_Morse = malloc(N_buffer * sizeof(char));
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	/****************************************** DÃ©clarations ******************************************/
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	char Mode = 'a'; //MODE
	int fin = 1; // variable pour fermer l'application
	///////////////////////////////////////////////////////////////////////////////////////////////////
	/****************************************** Application ******************************************/
	//////////////////////////////////////////////////////////////////////////////////////////////////
	while (fin == 1)
	{
		system("clear"); //nettoyage de l'interface
		printf("Bienvenu!!\nVeuillez Selectionner le mode de fonctionnement:\n\tE - Emetteur\n\tR - Recepteur\n\tT - Repetiteur\n Mon choix: ");
		//scanf("%[^\n]",&Mode);
		scanf("%c",&Mode);
		getchar(); // Pour vider le buffer

		/************ ExÃ©cution en mode Transmission ************/
		if (Mode == 'e' || Mode == 'E')
		{
			system("clear");

			printf("Saisissez votre message:\n");
			scanf("%[^\n]",Msg_raw);
			getchar();// Vider le buffer

			StringToMorse(Msg_raw,Msg_Morse);//Convertir le message en Morse
			printf("Envoi du code morse en cours:\t,%s,\n", Msg_Morse);
			//******Envoi du code morse au driver
			ret_val = ioctl (fd , MYDR_STOP , Msg_Morse);
			if (ret_val < 0) {
			printf("ioctl_send_msg failed:%d\n", ret_val);
			exit(-1);}

			write( fd , Msg_Morse , strlen(Msg_Morse) );

			close(fd);

			flushstring(Msg_Morse);
			flushstring(Msg_raw);
		}

		/************ ExÃ©cution en mode RÃ©ception ************/
		else if (Mode == 'r' || Mode == 'R')
		{
			system("clear");

			/* testing printf("Saisissez votre message:\n");
			scanf("%[^\n]",Msg_Morse);
			getchar();// Vider le buffer*/
			rd = open("/dev/My_Binaire",ORDWR);
			if (rd < 0){
				printf("binaire failed:%d\n", ret_val);
				exit(-1);}

			read(rd,Msg_Morse,1);
			printf("Code morse recu: %s\n", Msg_Morse);
			MorseToString(Msg_Morse,Msg_raw);
			printf("Message decode: %s\n", Msg_raw);

			close(rd);
			flushstring(Msg_Morse);
			flushstring(Msg_raw);
		}

		/****Execution en mode Repeteur****/
		else if (Mode == 't' || Mode == 'T')
		{
			system("clear");
			//ioctl read
			rd = open("/dev/My_Binaire",ORDWR);
			if (rd < 0){
				printf("binaire failed:%d\n", ret_val);
				exit(-1);}

			read(rd,Msg_Morse,1);
			close(rd);

			/*** Affichage pour le fun ***/
			printf("Code morse reçu: %s\n", Msg_Morse);
			MorseToString(Msg_Morse,Msg_raw);
			printf("Transmission du Message : %s",Msg_raw);

			//ioctl write
			ret_val = ioctl (fd , MYDR_STOP , Msg_Morse);
			if (ret_val < 0) {
			printf("ioctl_send_msg failed:%d\n", ret_val);
			exit(-1);}

			write( fd , Msg_Morse , strlen(Msg_Morse));
			close(fd);

			flushstring(Msg_Morse);
			flushstring(Msg_raw);
		}
		/*************** Fermer l'app? *****************/
		printf("Voulez vous continuer a jouer avec la Rpi?\n\t 1- Oui\n\t 2- Non\n");
		scanf("%d",&fin);
		getchar();



	}
	/**** LibÃ©rer la mÃ©moire ****/
	free(Msg_raw);
	free(Msg_Morse);
	return 0;
}



void StringToMorse(char *Msg_in, char *Msg_out)
{
	int size_in = strlen(Msg_in); //Taille du Message en entrÃ©e
	int index_Msg_in = 0;
	int index_Lookup_Table = 0;

	while (Msg_in[index_Msg_in])
	{
		Msg_in[index_Msg_in] = toupper(Msg_in[index_Msg_in]);
		index_Msg_in++;
	}
	index_Msg_in = 0;

	/**** Construct Morse Code ****/
	while ( index_Msg_in < size_in)
	{
		/***********************Lookup for the word in the lookup table*************************/
		//printf("Index_Msg_in = %d\n", index_Msg_in);
		while ( index_Lookup_Table < SIZE_LT)
		{
			if (Msg_in[index_Msg_in] == raw_table[index_Lookup_Table])
			{
				if (index_Lookup_Table == SIZE_LT-1) // dernier Ã©lÃ©ment de la table (" ")
				{
					Msg_out[strlen(Msg_out)-1] = '/';
					break;
				}
			else if (index_Msg_in == (size_in-1)) //dernier Ã©lÃ©ment de la chaine
			{
						strcat(Msg_out, morse_table[index_Lookup_Table]);
						break;
			}
			else if (index_Msg_in >0)
			{
						strcat(Msg_out, morse_table[index_Lookup_Table]);
						strcat(Msg_out," ");
						break;
			}
			else
			{
						strcpy(Msg_out, morse_table[index_Lookup_Table]);
						strcat(Msg_out," ");
						break;
					}
			}
			index_Lookup_Table++;
		}

		index_Msg_in++;
		index_Lookup_Table = 0;

	}
};

void MorseToString(char* Msg_in, char* Msg_out)
{
	char * Msg_out_tmp;
	Msg_out_tmp = malloc(N_buffer * sizeof(char));
	int size_in = strlen(Msg_in); //Taille du Message en entrÃ©e
	char buffer[10];
	int tmp = 0; //Utile pour parcourir une seule fois ;)
	int size_buffer = 0;
	char espace = ' ';
  int i = 0;
	int buffer_ready = 0;
	int up = 0;
	/******************** RÃ©cupÃ©ration du code lettre par lettre **********************/
	//for (int i =0 ; i < size_in; i++)
	//{
	while (i<size_in)
	{
		if ((Msg_in[i]==' ') || (i == size_in-1) || (Msg_in[i]=='/')) //lettres ou chiffres sÃ©parÃ©s soit d'un espace ou d'un / ou sinon, si c'est le dernier mot i ==size_in
			{
				if(i == size_in - 1) size_buffer = i - tmp + 1; //Pour ne pas rater le dernier caractere de l'entrée
				else size_buffer = i - tmp  ; // RÃ©cupÃ©ration de la taille du mot Ã  dÃ©coder
						for(int j = 0; j < size_buffer;j++)
						{
							if (tmp == 0)
							{
								buffer[j] = Msg_in[j]; // 1er buffer
								up++;
							}
							else buffer[j] = Msg_in[j+tmp]; //buffers restants
						}
						tmp = i+1;
						buffer_ready = 1;
			}
		buffer[size_buffer]= '\0';

			/**** DO stuff with the buffer ****/
			if(buffer_ready)
			{/*** lookup for the morse code in LT and copy it into Msg_out_tmp ***/
			for(int h = 0; h< SIZE_LT;h++)
			{
				if(!strcmp(buffer,morse_table[h]))
				{
					if(up==0)//1ere lettre
					{
						strncpy(Msg_out_tmp,&raw_table[h],1);
						break;
					}
					else
					{
						strncat(Msg_out_tmp,&raw_table[h],1);
						break;
					}
				}
			}
			 //put a space when there's a '/'
			 if(Msg_in[i]=='/') strncat (Msg_out_tmp,&espace,1);
			 /***** Clean buffer *****/
			 flushstring(buffer);
			 }
			 buffer_ready = 0;
			 i++;
	}
	printf("\n");
	strcpy(Msg_out,Msg_out_tmp);
	free(Msg_out_tmp);
	};

void flushstring(char* string)
{
//	string[0]='\0';
		//for (int i = strlen(string)-1; i> 0;i--)
		for(int i = 0; i< strlen(string); i++) string[i]='\0';
	};
