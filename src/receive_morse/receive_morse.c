#include <linux/module.h> /* Needed by all modules */
#include <linux/kernel.h> /* Needed for KERN_INFO */
#include <linux/init.h> /* Needed for the macros */
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/string.h>
#include "./../../headers/receive_morse.h"
#include <linux/ktime.h>
#include <linux/timer.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/interrupt.h>


static struct file_operations fops = 
{
	.read = my_binaire_read,
	.open = my_binaire_open,
	.write = my_binaire_write,
	.release = my_binaire_rls, 
};


/***************************************
 *  Conversion du binaire vers des 
 *  caractères qui représentes le code 
 *  morse avant de l'envoyer ver
 *  l'application 
 * ************************************/

int bin_to_morse(void)
{
	//taille_bin = 36;
	if (cluck == 0)
	{
		cluck = 1;
		count=0;
		while(count < taille_bin)
		{
			if (bufb[count]==1 && bufb[count+1]==1 && bufb[count+2]==1 && bufb[count+3]==0)
			{	 
				bufk[countchar]=decode[0];
				count = count +4;
				countchar = countchar +1 ;
			}	
			else if (bufb[count]==1 && bufb[count+1]==0 )
			{ 
				bufk[countchar]=decode[1];
				count = count +2;
				countchar = countchar +1 ;
			}		
			else if (bufb[count]==0 && bufb[count+1]==0 && bufb[count+2]==0 && bufb[count+3]==0 && bufb[count+4]==0 && bufb[count+5]==0  )
			{ 
				bufk[countchar]=decode[3];
				count = count +6;
				countchar = countchar +1 ;
			}			
			else if (bufb[count]==0 && bufb[count+1]==0)
			{ 
				bufk[countchar]=decode[2];
				count = count +2;
				countchar = countchar +1 ;
			}else
			{
				bufk[countchar]='N';
				count =count +1;
				countchar = countchar + 1;
			}	
		}
		taille_bin = 0;
		cluck = 0;
		return 1;	
	}else
	{
		return 0;
	}

}


/***************************************
 *  Reception du code binnaire des GPIO
 *  Remplissage du buffer binaire
 * ************************************/

static irqreturn_t receive_morse (int irq, void *data)
{
	disable_irq(RPI_GPIO_IN);
	printk ( KERN_NOTICE "<....> Lecture de l'état du GPIO  ");
	/*count = 0;
	//bluck = 1;
	printk ( KERN_NOTICE "<....> Lecture de l'état du GPIO  ");
	while (count == -1)
	{
		if (1 == gpio_get_value(RPI_GPIO_IN))
		{
			bufb[taille_bin] = 1;
			taille_bin = taille_bin + 1;
			count = 0;
		}else if (0 == gpio_get_value(RPI_GPIO_IN))
		{
			count++;
			if (count <14)
			{
				bufb[taille_bin] = 0;
				taille_bin = taille_bin + 1;
			}else
			{
				count = -1;
			}
		}		
	}
	//bluck = 0 ;
	while(0 == bin_to_morse());*/
	printk ( KERN_NOTICE "<1....> tat du GPIO  ");
	printk ( KERN_NOTICE "<2....> tat du GPIO  ");
	printk ( KERN_NOTICE "<3....> tat du GPIO  ");
	printk ( KERN_NOTICE "<4....> tat du GPIO  ");
	printk ( KERN_NOTICE "<5....> tat du GPIO  ");
	printk ( KERN_NOTICE "<6....> tat du GPIO  ");
	printk ( KERN_NOTICE "<7....> tat du GPIO  ");
	printk ( KERN_NOTICE "<8....> tat du GPIO  ");
	enable_irq(RPI_GPIO_IN);
	printk ( KERN_NOTICE "<....> tat du GPIO  ");
	return IRQ_HANDLED;
}



/***************************************
 *  Initialisation et chargement du 
 *  module
 * ************************************/

static int __init receive_morse_init (void) 
{	
	printk (KERN_CRIT " <....> initialisation  \n");
	
	err = register_chrdev(91,"My_Binaire", &fops);
	if (err<0)printk(KERN_ALERT " <  NO> registration\n");
	else printk(KERN_ALERT "<OK  >registration\n");

	gpio_request(RPI_GPIO_IN,THIS_MODULE->name);
	gpio_direction_input(RPI_GPIO_IN);
	gpio_export(RPI_GPIO_IN,0);

	err = request_irq(gpio_to_irq(RPI_GPIO_IN),receive_morse,IRQF_SHARED | IRQF_TRIGGER_RISING,THIS_MODULE->name,THIS_MODULE->name);
	if (err){printk (KERN_ERR "PAS de irq: %d \n",err);}

	printk (KERN_CRIT " <OK  > initialisation \n");


	return 0;

}

/***************************************
 *  Déchargement du module
 * ************************************/

static void __exit receive_morse_quit (void) 
{
	printk (KERN_CRIT " <....> déchargement du module \n");
	unregister_chrdev (91,"My_Binaire");
	free_irq(gpio_to_irq(RPI_GPIO_IN),THIS_MODULE->name);
	gpio_free(RPI_GPIO_IN);
	printk (KERN_CRIT " <OK  > déchargement du module \n");
}


/***************************************
 *  Gestion du device dédié au module
 * ************************************/

static ssize_t my_binaire_read (struct file * fil, char * buff, size_t len, loff_t * off)
{
	ssize_t ret = 0;
	if (cluck == 0)
	{	
		cluck = 1;
		printk (KERN_ALERT "read\n");
		
	//	while(0==bin_to_morse());
		
		if (*off == 0) 
		{
			if (copy_to_user(buff, bufk, sizeof(bufk))) 
			{
				ret = -EFAULT;
			} else 
			{
				ret = sizeof(bufk);
				*off = 1;
			}
		}
		countchar = 0;
		cluck = 0;
		return ret;
	}else
	{
		return -1;
	}
}

static int my_binaire_open (struct inode * inod, struct file * fil)
{
	printk (KERN_ALERT "open\n");
	return 0;
}

static ssize_t my_binaire_write (struct file * fil, const char * buff, size_t len, loff_t * off)
{
	int l = 0, m, mylen;
	printk (KERN_ALERT "write ");
	if (len > taille_max_buffer - 1) 
	{
		mylen = taille_max_buffer - 1;
	} else 
	{
		mylen = len ;
	}
	for (l = 0; l < mylen ; l++) 
	{
		get_user (bufk[l], buff + l);
	}
	for (m = mylen; m < taille_max_buffer; m++) 
	{
		bufk[m] = 0;
	}
	return mylen;
}

static int my_binaire_rls (struct inode * inod, struct file * fil)
{
	printk (KERN_ALERT "bye\n");
	return 0;
}

module_init (receive_morse_init);
module_exit (receive_morse_quit);

MODULE_AUTHOR("MANI_AITSAID");
MODULE_DESCRIPTION("Module de transmission/reception de code morse");
MODULE_LICENSE("GPL");
