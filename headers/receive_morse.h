#include <linux/module.h> /* Needed by all modules */
#include <linux/kernel.h> /* Needed for KERN_INFO */
#include <linux/init.h> /* Needed for the macros */
#include <linux/fs.h>
#include <linux/uaccess.h>


#define RPI_GPIO_OUT 2
#define RPI_GPIO_IN 4 
#define taille_max_buffer 50
#define taille_max_bin 200

//int bufb[36]={1,0,1,0,0,0,0,0,0,0,1,0,1,0,1,1,1,0,0,0,1,1,1,0,1,1,1,0,1,0,0,0,0,0,0,0};
char bufk [taille_max_buffer] = "code morse";
static int decode[4] = {'-','.',' ','/'};
int bufb [taille_max_bin];
int countchar=0;
int taille_bin;
int cluck = 0;
int bluck = 0;
int count=0;
int err = 0;

static ssize_t my_binaire_read (struct file * fil, char * buff, size_t len, loff_t * off);
static int my_binaire_open (struct inode * inod, struct file * fil);
static ssize_t my_binaire_write (struct file * fil, const char * buff, size_t len, loff_t * off);
static int my_binaire_rls (struct inode * inod, struct file * fil);


