#include <linux/module.h> /* Needed by all modules */
#include <linux/kernel.h> /* Needed for KERN_INFO */
#include <linux/init.h> /* Needed for the macros */
#include <linux/fs.h>
#include <linux/uaccess.h>


#define RPI_GPIO_OUT 2
#define RPI_GPIO_IN 4 
#define taille_max_buffer 50
#define taille_max_bin 200

static int code_1[4]= {1,1,1,0};
static int code_2[2]={1,0};
static int code_3[2]={0,0};
static int code_4[6]={0,0,0,0,0,0};
char kbuf [taille_max_buffer] = "code morse";
int bbuf [taille_max_bin];
int taille_char = 0;
int count_bin=0;
int sluck = 0;
int luck = 0;
int err = 0;
int i;

static ssize_t my_morse_read (struct file * fil, char * buff, size_t len, loff_t * off);
static int my_morse_open (struct inode * inod, struct file * fil);
static ssize_t my_morse_write (struct file * fil, const char * buff, size_t len, loff_t * off);
static int my_morse_rls (struct inode * inod, struct file * fil);

