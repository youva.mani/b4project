#include <linux/module.h> /* Needed by all modules */
#include <linux/kernel.h> /* Needed for KERN_INFO */
#include <linux/init.h> /* Needed for the macros */
#include <linux/fs.h>
#include <linux/uaccess.h>


#define taille_max_buffer 16
#define MYDRBASE 'k' 
#define MYDR_RESET _IO( MYDRBASE, 1)
#define MYDR_STOP _IO( MYDRBASE, 2)


static int param;
static int param_array[2]={10,45};
static int arr_argc = 0; 

static long dev_ioctl ( struct file *f , unsigned int cmd , unsigned long arg ); //actuel
static int dev_open(struct inode * inod , struct file * fil);
static ssize_t dev_read (struct file * fil, char * buff, size_t len, loff_t *off);
static ssize_t dev_write (struct file * fil, const char * buff, size_t len,loff_t * off);
static int dev_rls (struct inode * inod, struct file * fil);

static struct file_operations fops = 
{
	.read = dev_read,
	.open = dev_open,
	.unlocked_ioctl = dev_ioctl,
	.write = dev_write,
	.release = dev_rls, 
};


struct timer_list exp_timer;

void do_something (struct timer_list *timer) {
	printk (KERN_ALERT "Yop !");
	mod_timer (timer , jiffies + 2 * HZ); //réarmement du Timer
}




char kbuf [taille_max_buffer] = "Hello read\n\0"; 


static long dev_ioctl ( struct file *f , unsigned int cmd , unsigned long arg );

static int __init hello_start (void) {
	printk (KERN_CRIT " Hello \n");
	printk(KERN_DEBUG "param = %d \n ", param);
	printk(KERN_DEBUG "heure : %d \t minute : %d \n", param_array[0],param_array[1]); 
	int t = register_chrdev(90,"My_Device", &fops);
	if (t<0)printk(KERN_ALERT "registration failed\n");
	else printk(KERN_ALERT "registration success\n");

	timer_setup (&exp_timer, do_something, 0);
	mod_timer (&exp_timer , jiffies + 2 * HZ);

	return t;
}
static void __exit hello_end (void) {
	printk (KERN_CRIT " Goodbye \n");
	unregister_chrdev (90,"My_Device"); 
	del_timer (&exp_timer);
}

static int dev_rls (struct inode * inod, struct file * fil){
	printk (KERN_ALERT "bye\n");
	return 0;
}
static int dev_open (struct inode * inod, struct file * fil){
	printk (KERN_ALERT "open\n");
	return 0;
}

static ssize_t dev_read (struct file * fil, char * buff, size_t len, loff_t * off)
{
	ssize_t ret = 0;
	printk (KERN_ALERT "read\n");
	if (*off == 0) {
		if (copy_to_user(buff, kbuf, sizeof(kbuf))) {
			ret = -EFAULT;
		} else {
			ret = sizeof(kbuf);
			*off = 1;
		}
	}
	return ret;
}


static ssize_t dev_write (struct file * fil, const char * buff, size_t len, loff_t * off){
	int l = 0, m, mylen;
	printk (KERN_ALERT "write ");
	if (len > taille_max_buffer - 1) {
		mylen = taille_max_buffer - 1;
	} else {
		mylen = len ;
	}
	for (l = 0; l < mylen ; l++) {
		get_user (kbuf[l], buff + l);
	}
	for (m = mylen; m < taille_max_buffer; m++) {
		kbuf[m] = 0;
	}
	printk (KERN_ALERT "%s \n", kbuf);
	return len ;
}

static long dev_ioctl ( struct file *f , unsigned int cmd , unsigned long arg ){
	switch ( cmd ) {
		case MYDR_RESET:
			printk (KERN_ALERT "MYDR_RESET %lu", arg);
			break ;
		case MYDR_STOP:
			printk (KERN_ALERT "MYDR_STOP %lu", arg);
			break ;
		default :
			printk (KERN_ALERT "unknown ioctl");
			break ;
	}
	return 0;
}



module_param(param,int,0);
module_param_array(param_array, int,&arr_argc,0000);
module_init (hello_start);
module_exit (hello_end);


MODULE_PARM_DESC(param," Un parametre de ce module");
MODULE_AUTHOR("MANI_AITSAID");
MODULE_DESCRIPTION("We're just saying hello & good bye ;)");
MODULE_LICENSE("GPL");
